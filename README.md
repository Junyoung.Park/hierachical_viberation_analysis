# Hierachical Vibration Data Analytics

## Getting Started


### Prerequisites

- Python 3.x
- Numpy 

### Quick start

```
$ git clone https://gitlab.com/Junyoung.Park/hierachical_viberation_analysis.git
$ cd hierachical_viberation_analysis
$ pip install numpy
```

### Repository structure

- scripts
    - gateway_utils.py : contains gateway analytics related codes

- docs
    - CMS_UML.pdf: contains UML for sensor node - gateway configuration