import numpy as np
from scripts.gateway_utils import infer_raw_data
from scripts.generate_model import GatewayModel

if __name__ == '__main__':
    " Script for initializing and saving model"
    # model = GatewayModel()
    # model.save(model_path)

    # Generate random data
    data = np.random.rand(1, 8192)

    # Load model from the disk
    model_path = './model_saves/demo_model'
    model = GatewayModel.load(model_path)

    # Analyze input data
    result = infer_raw_data(data, model)

    print(result)