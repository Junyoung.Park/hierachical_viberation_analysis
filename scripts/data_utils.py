import numpy as np

def getFloatValue(adc_value, adcMax=4096, adcMin=0, dMax=50, dMin=-50)
    """
        adc_value : ADC reading value(s)
        adcMax : Max value of ADC
        adcMin : Min value of ADC
        dMax : Maximum sensor measureable value
        dMin : Minimum sensor measureable value
        return: converted acceleration values(s)
    """
    
    adc_value * (dMax-dMin)