import numpy as np


def infer_raw_data(raw_data, model=None):
    """
    :param raw_data: 1d array which contains vibration signal in time domain.
    :param model: deep learning model. written in pytorch model.
    :return: inference result. integer-valued.
    """
    # ensure the input signal to be 2d to be compatible with model's input spec.
    raw_data = np.array(raw_data)
    raw_data = raw_data.reshape(1, -1)

    if model is None: # Demonstration purpose
        model = demo_model

    # Run deep learning model and get inference result
    # whether the given time-series is normal or abnormal
    # if 'inference_result' is 0, then the signal is normal
    # otherwise the signal is abnormal
    inference_result = model(raw_data)
    return inference_result


def demo_model(*args):
    # dumb way for simulating deep learning module
    # only match output format
    random_result = np.random.random_integers(low=0, high=3, size=1)
    return random_result
