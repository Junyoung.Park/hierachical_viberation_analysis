import torch
import json


class GatewayModel(torch.nn.Module):

    def __init__(self,
                 input_dimension=8192,
                 output_dimension=4):

        """
        Untrained neural network for simulating vibration analysis scenario on gateway.
        """

        super().__init__()
        self.layers = torch.nn.ModuleList()
        self.input_dimension = input_dimension
        self.output_dimension = output_dimension
        self.out_layer = torch.nn.Softmax(dim=1)

        self.layers.append(torch.nn.Linear(self.input_dimension, 128))
        self.layers.append(torch.nn.Linear(128, self.output_dimension))

    def _forward(self, x):
        """
        x : vibration signal in (g). data shape is [Batch_dim x input_dimension]
        """
        x = self.to_tensor(x)
        for layer in self.layers:
            x = layer(x)
        x = self.out_layer(x)
        return x

    def forward(self, x):
        with torch.no_grad():
            logits = self._forward(x)
        labels = torch.argmax(logits, dim=1)  # Pytorch tensors
        labels = labels.numpy().tolist()  # Convert to list of integers
        return labels

    @staticmethod
    def to_tensor(x):
        if not isinstance(x, torch.Tensor):
            x = torch.Tensor(x)
        return x

    def get_attributes(self):
        param_dict = {}
        for k in self.__dict__.keys():
            if k[:1] != '_':
                param_dict[k] = getattr(self, k)
        return param_dict

    def save(self, save_path):
        weight_path = save_path + '.pt'
        param_path = save_path + '.json'

        # save model weights
        torch.save(self.state_dict(), weight_path)

        # save model parameters
        param_dict = self.get_attributes()
        param_dict.pop('training')
        with open(param_path, 'w') as f:
            json.dump(param_dict, f)

        print("Model saved")

    @classmethod
    def load(cls, load_path):
        weight_path = load_path + '.pt'
        param_path = load_path + '.json'

        with open(param_path) as f:
            j = json.load(f)

        model_loaded = cls(**j)
        model_loaded.load_state_dict(torch.load(weight_path))
        model_loaded.eval()  # switch model to evaluation mode
        return model_loaded


