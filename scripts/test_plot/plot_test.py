import scipy.io
import numpy as np
from plot_utils import plot_result
from time import sleep
import matplotlib.pyplot as plt

dat = scipy.io.loadmat('105.mat')
dat_fe = dat['X105_FE_time']
dat_rpm = dat['X105RPM']
dat_ba = dat['X105_BA_time']
dat_de = dat['X105_DE_time']
g2 = np.random.randint(0, 10000, 100)
p=[0.8,0.1,0.05,0.05]

for i in range(int(len(dat_fe)/100)):
	data = dat_fe[i:i+100]
	label = np.random.choice(4,p=p)
	print("label : ", label)
	plot_result(data, g2, label)
	plt.pause(1)
