import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib import colors

fig = plt.figure(figsize=(10,5))

def plot_result(raw_dat, fft_dat, label, num_label = 4):
	colormap = [colors.to_rgba('g'),colors.to_rgba('r'),colors.to_rgba('y'),colors.to_rgba('k')]
	gridsize = (math.ceil(num_label/2),4)

	ax_raw = plt.subplot2grid(gridsize, (0,0), colspan = 2, rowspan = int(num_label/4))
	ax_raw.set_title('Raw Data')
	ax_fft = plt.subplot2grid(gridsize, (int(num_label/4),0), colspan = 2, rowspan = int(num_label/4))
	ax_fft.set_title('FFT Result')
	ax_l = [None]*num_label
	for i in range(num_label):
		ax_l[i] = plt.subplot2grid(gridsize, (int(i/2), 2+i%2))
		ax_l[i].imshow([[colors.to_rgba('gray')]], interpolation=None)
		ax_l[i].axis('off')

	ax_raw.plot(raw_dat)
	ax_fft.stem(fft_dat)
	ax_l[label].imshow([[colormap[label]]])
	
	plt.tight_layout()
	fig.canvas.draw()
	fig.canvas.flush_events()
